# Тестовое задание на позицию Junior QA Engineer #

### Задание
* Написать такой же тест, с такой же структурой проекта (https://bitbucket.org/pvivanov/yandexweather/src/master/), только для API Яндекс.Переводчик
* Тест должен проверять перевод с английского “Hello World!” на русский “Всем Привет!”

### Summary

* Выдача бесплатных API-ключей для API Яндекс.Переводчика приостановлена, поэтому в YandexTranslateGateway добавлен метод генерации IAM-токена, необходимого для формирования запроса. 
* Для тестирования перевода текста “Hello World!” на русский “Всем Привет!” был использован запрос с применением глоссария.

### Presets
* install jdk8
* install lombok plugin
* customize lombok plugin (Build, Execution, Deployment --> Compiler --> Annotation Processors, Enable annotation processing)
* enjoy...