package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Result;
import ru.testing.gateway.YandexTranslateGateway;

public class YandexTranslateTest {
    private static final String[] TEXT = {"Hello World!"};
    private static final String GLOSSARY_SOURCE = "Hello World!";
    private static final String GLOSSARY_TRANSLATED = "Всем Привет!";

    @Test
    @DisplayName("Перевод текста с использованием словаря")
    public void getTranslationWithGlossary() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        Result translation = yandexTranslateGateway.getTranslation(TEXT, GLOSSARY_SOURCE, GLOSSARY_TRANSLATED);
        Assertions.assertEquals("Всем Привет!", translation.getTranslations().get(0).getText());
    }
}