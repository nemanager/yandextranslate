package ru.testing.gateway;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import ru.testing.entities.*;

import java.util.Collections;

@Slf4j
public class YandexTranslateGateway {
    private static final String URL_TRANSLATE = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String URL_IAM_TOKEN = "https://iam.api.cloud.yandex.net/iam/v1/tokens";
    private static final String TOKEN = "AQAAAAAfhPNxAATuwfHmyEXCq0scvmgZHj9a4vY";
    private static final String SOURCE_LANGUAGE = "en";
    private static final String TARGET_LANGUAGE = "ru";
    private static final String FOLDER_ID = "b1g1ul9qt1j1pp96bg96";

    @SneakyThrows
    public static String getIamToken() {
        HttpResponse<JsonNode> response = Unirest.post(URL_IAM_TOKEN)
                .header("Accept", "application/json")
                .body(new JSONObject()
                        .put("yandexPassportOauthToken", TOKEN)
                        .toString())
                .asJson();
        return response.getBody().getObject().getString("iamToken");
    }

    @SneakyThrows
    public Result getTranslation(String[] text, String glossarySourceText, String glossaryTranslatedText) {
        Gson gson = new Gson();

        GlossaryPair glossaryPair = GlossaryPair.builder()
                .sourceText(glossarySourceText)
                .translatedText(glossaryTranslatedText)
                .build();

        GlossaryData glossaryData = GlossaryData.builder()
                .glossaryPairs(Collections.singletonList(glossaryPair))
                .build();

        GlossaryConfig glossaryConfig = GlossaryConfig.builder()
                .glossaryData(glossaryData)
                .build();

        Source source = Source.builder()
                .sourceLanguageCode(SOURCE_LANGUAGE)
                .targetLanguageCode(TARGET_LANGUAGE)
                .texts(text)
                .folderId(FOLDER_ID)
                .glossaryConfig(glossaryConfig)
                .build();

        Gson gson1 = new GsonBuilder().setPrettyPrinting().create();
        String body = gson1.toJson(source);

        String token = "Bearer " + getIamToken();

        HttpResponse<String> response = Unirest.post(URL_TRANSLATE)
                .header("Content-Type", "application/json")
                .header("Accept", "*/*")
                .header("Authorization", token)
                .body(body)
                .asString();
        String strResponse = response.getBody();
        log.info("Response: " + strResponse);
        return gson.fromJson(strResponse, Result.class);
    }
}