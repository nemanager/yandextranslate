package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

// Сборка объекта для тела запроса
@Getter
@Builder
public class Source {
    @SerializedName("sourceLanguageCode")
    public String sourceLanguageCode;
    @SerializedName("targetLanguageCode")
    public String targetLanguageCode;
    @SerializedName("texts")
    public String[] texts;
    @SerializedName("folderId")
    public String folderId;
    @SerializedName("glossaryConfig")
    public GlossaryConfig glossaryConfig;
}