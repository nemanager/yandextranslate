package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GlossaryConfig {
    @SerializedName("glossaryData")
    public GlossaryData glossaryData;
}