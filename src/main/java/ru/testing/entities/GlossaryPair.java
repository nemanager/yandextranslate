package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GlossaryPair {
    @SerializedName("sourceText")
    public String sourceText;
    @SerializedName("translatedText")
    public String translatedText;
}