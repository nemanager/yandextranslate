package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

// Сборка объекта для тела ответа
@Getter
@Builder
public class Translation {
    @SerializedName("text")
    public String text;
}
